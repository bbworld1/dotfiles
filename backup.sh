#!/bin/bash
# Backup dotfiles
set -e

tar -C /home/bbworld --exclude=.config/discord --exclude=.config/coc/extensions --exclude=.config/BraveSoftware --exclude=.config/unity3d/cache  -czvf config.tar.gz .config/
tar -C /home/bbworld --exclude=.local/lib/python3.8/site-packages --exclude=.local/share/TelegramDesktop -czvf local.tar.gz .local/
tar -C /home/bbworld -czvf vimfiles.tar.gz .vim/
tar -C /home/bbworld -czvf misc.tar.gz .bashrc .profile .Xresources
